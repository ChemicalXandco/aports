# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-astroid
pkgver=2.12.11
pkgrel=0
pkgdesc="A new abstract syntax tree from Python's ast"
url="https://pylint.pycqa.org/projects/astroid/en/latest/"
arch="noarch"
license="LGPL-2.1-or-later"
depends="python3 py3-lazy-object-proxy py3-wrapt"
replaces="py-logilab-astng"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-typing-extensions"
source="py3-astroid-$pkgver.tar.gz::https://github.com/PyCQA/astroid/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/astroid-$pkgver"

build() {
	mkdir dist
	python3 -m gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/astroid-*.whl
}

sha512sums="
1e8e364bcebce90d06f90494e2a029bfeae89a0dd3120400fa6ff5098ca05ec7c7f520336cc3b174ac007c81e272d774385c35f94762789b767d38e603c7d41d  py3-astroid-2.12.11.tar.gz
"
