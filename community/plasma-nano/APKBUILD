# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-nano
pkgver=5.26.0
pkgrel=0
pkgdesc="A minimal Plasma shell package intended for embedded devices"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/kde/plasma-nano"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="qt5-qtvirtualkeyboard"
makedepends="
	extra-cmake-modules
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-nano-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
77008383a5f436601b73a0a045dbb470e115278e34b3f09addf30e74d5efcb1e94b18c3e0d5c2d668052b4c48603b4fa71fd0f4ba144675e09a625f6ed142aaf  plasma-nano-5.26.0.tar.xz
"
