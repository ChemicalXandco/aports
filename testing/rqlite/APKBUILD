# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=rqlite
pkgver=7.7.2
pkgrel=0
pkgdesc="Lightweight, distributed relational database built on SQLite"
url="https://github.com/rqlite/rqlite"
arch="all !riscv64" # ftbfs
license="MIT"
makedepends="go"
checkdepends="python3 py3-requests"
options="!check" # a bunch of them fail due to runtime requirements
subpackages="
	$pkgname-doc
	$pkgname-bench
	$pkgname-client
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rqlite/rqlite/archive/refs/tags/v$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -modcacherw -trimpath"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/rqbench
	go build ./cmd/rqlite
	go build ./cmd/rqlited
}

check() {
	RQLITED_PATH="$builddir"/rqlited python3 system_test/full_system_test.py
}

package() {
	install -Dm755 rqbench rqlite rqlited \
		-t "$pkgdir"/usr/bin

	install -D -m644 "$builddir"/DOC/*.md -t \
		"$pkgdir"/usr/share/doc/$pkgname
}

bench() {
	pkgdesc="$pkgdesc (benchmark utility)"

	amove usr/bin/rqbench
}


client() {
	pkgdesc="$pkgdesc (client)"

	amove usr/bin/rqlite
}

sha512sums="
6f5756005d72f1c97b71866b3f341ec5959f53178cf99c6bb3315bb45cf44e81c4d2807aebd3ccc858e2c6b9e3cc7d1e9a86c3f6c3dd6272ab3f7fb7550a632f  rqlite-7.7.2.tar.gz
"
